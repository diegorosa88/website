package calculator

import "testing"

func TestSum(t *testing.T) {
	t.Run("sum two simple numbers", func(t *testing.T) {
		var n1 int64 = 10
		var n2 int64 = 20
		result := Sum(n1, n2)
		if result != 30 {
			t.Fatalf("expected to be 30, but was [%v]", result)
		}
	})
}
