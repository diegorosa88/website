package main

import (
	"net/http"
	"os"

	"github.com/labstack/echo"
)

func main() {
	port := os.Getenv("PORT")
	e := echo.New()

	e.Logger.Info("starting at port = " + port)

	e.File("/home", "templates/home.html")
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.Logger.Fatal(e.Start(":" + port))

	// https://medium.com/swlh/how-do-i-deploy-my-code-to-heroku-using-gitlab-ci-cd-6a232b6be2e4
	// https://gitlab.com/michelleamesquita/devsecops2/-/blob/main/.gitlab-ci.yml
	// https://devcenter.heroku.com/articles/build-docker-images-heroku-yml
}
